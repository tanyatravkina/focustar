/*���� �������, ���������� ��������� ��������:
  100
   96
   500
   987
   799
   80
   300
   657
��������� ��������, ������� ���������� ������� � ������ -  �96,300,799�
*/
/*DROP TABLE task2*/
CREATE TABLE task2 (
       some_num number (3))
/
DECLARE
  v#var  task2%ROWTYPE;
  i             NUMBER;
  TYPE num_array IS VARRAY(8) OF number(3); 
  team num_array := num_array(100, 96, 500, 987, 799, 80, 300, 657);                       
BEGIN
  FOR i IN 1..8 LOOP    
    INSERT INTO task2 VALUES (team(i));
  END LOOP;
END;
/
SELECT n 
FROM task2 
WHERE n NOT IN(
			   SELECT REGEXP_SUBSTR('96,300,799', '[^,]+', REGEXP_INSTR('96,300,799', '[^,]+', 1, LEVEL, 0), 1) t 
			   FROM DUAL
			   CONNECT BY REGEXP_INSTR('96,300,799', '[^,]+', 1, LEVEL) > 0
			   )