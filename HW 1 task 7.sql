/*���� ������� �������� GRATH_SRC � TEMP_GRATH � ��������� ������� ������ ������� ����� ������, ��������� � ���������� ������ �� ���� weight_src*/
CREATE TABLE grath_src(
       grath_src_id number (1),
       from_src number (3),
       to_src number (3),
       zeight_src number (3)
       );
INSERT INTO grath_src VALUES (1, 100, 102, 100);
INSERT INTO grath_src VALUES (2, 100, 101, 400);
INSERT INTO grath_src VALUES (3, 101, 102, 200);
INSERT INTO grath_src VALUES (4, 300, 301, 300);
INSERT INTO grath_src VALUES (5, 300, 302, 400);
INSERT INTO grath_src VALUES (6, 301, 302, 500);
/
CREATE TABLE temp_grath(
       from_temp number (3),
       to_temp number (3),
       weight number (3)
       );
INSERT INTO temp_grath VALUES (100, 101, 200);
INSERT INTO temp_grath VALUES (200, 201, 200);
INSERT INTO temp_grath VALUES (300, 301, 300);
INSERT INTO temp_grath VALUES (300, 302, 400);
INSERT INTO temp_grath VALUES (301, 302, 600);
/
SELECT gs.from_src, gs.to_src, gs.weight_src 
FROM grath_src gs
MINUS
SELECT tg.from_temp, tg.to_temp, tg.weight 
FROM temp_grath tg 
UNION(
	  SELECT t.from_temp, t.to_temp, t.weight 
	  FROM temp_grath tg 
	  WHERE tg.from_temp NOT IN(
								SELECT gs.from_src 
								FROM grath_src gs
								) 
	  );